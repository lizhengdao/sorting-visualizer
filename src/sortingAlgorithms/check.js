function insertionSort(array) {
    const animations = []
    for (let i = 0; i < array.length; i++) {
        var val = array[i];
        var hole = i;
        while (hole > 0 && array[hole - 1] > val) {

            array[hole] = array[--hole]
        }
        array[hole] = val
    }
    return array
}

console.log(insertionSort([5, 4, 3, 2, 1]))